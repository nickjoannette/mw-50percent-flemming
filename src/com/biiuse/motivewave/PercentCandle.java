package com.biiuse.motivewave;

import com.motivewave.platform.sdk.common.*;

import com.motivewave.platform.sdk.common.desc.*;
import com.motivewave.platform.sdk.common.desc.FileDescriptor;
import com.motivewave.platform.sdk.common.menu.MenuDescriptor;
import com.motivewave.platform.sdk.common.menu.MenuItem;
import com.motivewave.platform.sdk.common.menu.MenuSeparator;
import com.motivewave.platform.sdk.draw.Box;
import com.motivewave.platform.sdk.draw.Figure;
import com.motivewave.platform.sdk.draw.Marker;
import com.motivewave.platform.sdk.study.RuntimeDescriptor;
import com.motivewave.platform.sdk.study.Study;
import com.motivewave.platform.sdk.study.StudyHeader;
import com.motivewave.platform.sdk.draw.Label;


import java.awt.*;
import java.awt.geom.Path2D;
import java.awt.geom.Rectangle2D;
import java.io.*;
import java.nio.charset.Charset;
import java.text.ParseException;
import java.util.*;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

@StudyHeader(
        namespace="com.tradingindicatorsInsideBars",
        id="<50% Body Candle v1.0",
        rb="com.motivewave.platform.study.nls.strings2",
        name="<50% Body Candle",
        desc="Identifies candlesticks with a body size less than X% of the total high and low. Places a dot inside the candle.",
        menu="Flemming Dall",
        signals=false,
        overlay=true,
        requiresBarUpdates = false,
        studyOverlay=true
)

public class PercentCandle extends Study {

    enum Values {PercentBar};

    @Override
    public void initialize(Defaults defaults) {
        SettingsDescriptor sd = new SettingsDescriptor();
        SettingTab tab = new SettingTab("Style");
        sd.addTab(tab);
        setSettingsDescriptor(sd);

        MarkerDescriptor Marker = new MarkerDescriptor(Inputs.MARKER,"Marker", Enums.MarkerType.CIRCLE, Enums.Size.SMALL, new Color(255,255,255),
                new Color(255,255,255), true,true);

        SettingGroup setup = new SettingGroup("Input");
        setup.addRow(new DoubleDescriptor(Inputs.INPUT, "Minimal Body Size (%)", 50.0, 0, 100, 0.01));
        tab.addGroup(setup);

        SettingGroup marker = new SettingGroup("Marker");
        marker.addRow(Marker);
        tab.addGroup(marker);

        RuntimeDescriptor desc = new RuntimeDescriptor();
        desc.setLabelSettings(Inputs.INPUT);
        desc.exportValue(new ValueDescriptor(Values.PercentBar, "<% Body Candle", new String[] {Inputs.INPUT}));
        setRuntimeDescriptor(desc);
    }

    @Override
    public void calculateValues(DataContext ctx) {

        if (ctx == null) return;
        List<BarSize> barSizes = getBarSizes();
        if (!Util.isEmpty(barSizes)) {
            for (BarSize bs : barSizes) {
                DataSeries ds = ctx.getDataSeries(bs);
                if (ds == null || !ds.hasData()) return;
            }
        }

        clearFigures();
        DataSeries series = ctx.getDataSeries();
        Settings settings = getSettings();
        MarkerInfo m = settings.getMarker(Inputs.MARKER);
        Double minimumPercent = settings.getDouble(Inputs.INPUT);
        for (int i = 0; i < series.size(); i++) if (series.isBarComplete(i)) {
            Double high = series.getDouble(i, Enums.BarInput.HIGH, Double.NaN);
            Double low = series.getDouble(i, Enums.BarInput.LOW, Double.NaN);
            Double open = series.getDouble(i, Enums.BarInput.OPEN, Double.NaN);
            Double close = series.getDouble(i, Enums.BarInput.CLOSE, Double.NaN);
            Double MidCandleBody = (open+close)/2d;
            Double LowHighRange = (high-low)/100d*minimumPercent;
            Double CandleBodySize = Math.abs(open-close);
            Double DrawPoint = CandleBodySize <= LowHighRange ? MidCandleBody : Double.NaN;
            if (!DrawPoint.isNaN()) {
                Coordinate c = new Coordinate(series.getStartTime(i), DrawPoint);
                Marker mark = new Marker(c, m.getType(), m.getSize(), Enums.Position.TOP, m.getFillColor(), m.getOutlineColor());
                series.setDouble(i, Values.PercentBar, DrawPoint);
                if (m.isEnabled()) addFigure(mark);
            }
            series.setComplete(i);
        }
    }
}
